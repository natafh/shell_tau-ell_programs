% 140604 HCN : change echelle des temps suite correction erreur Omega
% CORE TURBULENCE
% ---------------
% petit programme de dessin des échelles de temps et de longueur
% pertinentes pour le noyau terrestre (chapitre TOG)
% 140604 HCN corrige erreur Omega vue par Jerome Noir
%		t_day = Omega^(-1)
%		day = 1 day
% 141105 HCN : change couleur u de k en c, et k en w pour day et Rossby
%
% *** ameliorer coude dissipation visqueuse ***
%
% dessin
% ------
h_core = figure('name','scales for Highway turbulence')
%
hold on
box on
% xlim([1.e-1 3480000]);
% xlim([1.e-1 R_o]);
% ylim([100 1.e13]);
xlim([1.e-2 R_o]);
%ylim([10 1.e13]);
ylim([10 1.e12]);
%
% intersection t_visc et t_spinup a l = R_o E^(1/2)
l_trans = R_o * Ekman^(1./4.) ; 
% times
% to suppress part of viscous line above t_spinup
for ii=1:length(t_nu)
	t_nu_rot(ii) = min(t_nu(ii),t_spinup(1));
	longueur_rot(ii) = min(longueur(ii),l_spinup(1));
end
loglog(longueur_rot, t_nu_rot, 'm','LineWidth',2) % temps de dissipation visqueuse
% loglog(longueur, t_nu, 'm','LineWidth',2) % temps de dissipation visqueuse
% loglog(longueur, t_spinup,':m','LineWidth',2); % ligne pointillée pour temps de spin-up
loglog(R_o, t_SV, 'oc','markersize',8,'MarkerFaceColor','k') % temps d'ancrage SV
% loglog(l_3D_SV, t_3D_SV, ':k','LineWidth',1) % temps des tourbillons 3D classiques (ancrage SV)
loglog(l_Rossby, t_Rossby, ':k','LineWidth',2) % temps des ondes de Rossby
loglog(l_spinup, t_spinup, 'm','LineWidth',2) % temps de spin-up
% loglog(l_nu_surf_TW, t_nu_surf_TW, ':m','LineWidth',2) % temps de dissipation TW aux bords
% loglog(longueur, t_sound, ':k') % temps des ondes sismiques
% 130604 HCN nouveau t_day
loglog(xlim, [t_day t_day],'-.k','LineWidth',2); % ligne dash-dot pour un Omega^(-1)
% loglog(longueur, t_2D,'-.k','LineWidth',2); % temps de mise en place de colonne géostrophique
% lengths
% loglog([l_K l_K], ylim, ':k'); % ligne pointillée pour longueur de Kolmogorov
% loglog([delta_E delta_E], ylim, '-.k'); % ligne dash-dot pour épaisseur couche d'Ekman
loglog([scale_height scale_height], ylim, '-.k'); % ligne dash-dot pour scale height
% loglog([rayon_Ro rayon_Ro],ylim,'-b'); % ligne pleine pour rayon de Rossby
%
% loglog(l_core_u, t_core_u, 'k','LineWidth',4) % spectre composite de vitesse
% en pointillé : prédiction 2D sous échelle d'injection : k^-5 (l^-1) jusqu'a t_nu
% [l_core_2D, t_core_2D] = function_intersect(l_core_u(2), t_core_u(2), -1., 1., 1./nu, 2.) ;
% loglog([l_core_2D l_core_u(2)], [t_core_2D t_core_u(2)], '--k','LineWidth',4) % spectre 2D pointillé
%
% loglog(l_core_u(3), t_core_u(3)/10., 'dk','LineWidth',4) % jet zonal à échelle de Rhines (10 x u non zonal...)
% loglog(l_jet_zonal, t_jet_zonal, 'dk','LineWidth',4) % jet zonal à échelle de Rhines
%
loglog(longueur, t_eta, 'b','LineWidth',2) % temps de dissipation Joule
loglog([eta/V_A R_o], [eta/V_A^2 t_A0], 'g','LineWidth',2) % temps des ondes d'Alfvén (new)
loglog(R_o, t_A0, 'or','markersize',8,'MarkerFaceColor','r') % temps d'ancrage Alfvén

if (0==1)
	if (scenario_MHD==1)
		loglog(l_core_u, t_core_u, 'c','LineWidth',4) % spectre composite de vitesse (remonté pour visibilité)
	else
		loglog(l_core_u, t_core_u, 'c','LineWidth',4) % spectre composite de vitesse
	end
	loglog(l_core_b(2:4), t_core_b(2:4), 'r','LineWidth',4) % spectre composite de vitesse d'Alfvén (ie de b) (sans bout diffusif)
else % real numerical tau-ell spectra
	dirname = '/data/geodynamo/natafh/tau-ell/TOG_IUGG_programs/Naths/';
	filename = 'tau_ell_step2.xlsx';
	[Nd, ell, tau_U, tau_B] = read_tau_ell(dirname, filename);
	ell = ell*R_o/ell(1); % recalage de ell_max sur le rayon du noyau R_o
	loglog(ell, tau_U, 'c','LineWidth',4) % spectre de vitesse
	loglog(ell, tau_B, 'r','LineWidth',4) % spectre de champ magnetique
end
%
% marqueurs puissance dissipée
loglog(l_nu_power, t_nu_power, 'sm','MarkerSize',10) % marqueurs puissance visqueuse dissipée
loglog(l_nu_TW, t_nu_TW, 'sm','MarkerSize',10,'MarkerFaceColor','m') % marqueurs TW visqueux
loglog(l_eta_power, t_eta_power, 'sb','MarkerSize',10) % marqueurs puissance ohmique dissipée
loglog(l_eta_TW, t_eta_TW, 'sb','MarkerSize',10,'MarkerFaceColor','b') % marqueur TW ohmique
%
% marqueurs puissance dissipée QG --> diamond
loglog(l_nu_QG_power, t_nu_QG_power, 'dm','MarkerSize',10) % marqueurs puissance visqueuse dissipée
loglog(l_nu_QG_TW, t_nu_QG_TW, 'dm','MarkerSize',10,'MarkerFaceColor','m') % marqueurs TW visqueux
%
% on ajoute ligne pointillée égale dissipation TW : tau(l) = l^-2/3
% point bas du spectre en b = l_core_b(3) et t_core_b(3)
% intersection pente tau(l) = l^-2/3 avec tau_eta --> FAUX
% 130404 : correction de Nath : pente horizontale (tau = cte)
[ll, tt] = function_intersect(l_core_b(3), t_core_b(3), 0., 1., 1./eta, 2.) ; % intersection pente egale dissipation et point bas spectre en b
loglog([ll l_core_b(3)], [tt t_core_b(3)], ':b','LineWidth',2)

%
% force memes echelles log sur les 2 axes
x_lim = xlim;
y_lim = ylim;
dumy=10.;
pbaspect([log10(x_lim(2)/x_lim(1)) log10(y_lim(2)/y_lim(1)) dumy]) ;
set(gca,'Xtick',[1.e-2 1. 1.e2 1.e4 1.e6]) ; 
set(gca,'Ytick',[1.e-2 1. 1.e2 1.e4 1.e6 1.e8 1.e10 1.e12]) ;
%
%
% annotations
%
if (0 == 0)
	text(l_nu_TW*0.8,t_nu_TW,'TW','Editing','on','HorizontalAlignment','right','color','m');
	text(l_nu_QG_TW*1.1,t_nu_QG_TW,' TW','Editing','on','HorizontalAlignment','left','color','m');
	text(l_eta_TW*0.7,t_eta_TW,'TW','Editing','on','HorizontalAlignment','right','color','b');
%	text(R_o,t_day,'\Omega^{-1}','Editing','on','HorizontalAlignment','left');
% 130604 HCN nouveau t_day
	text(R_o*1.5,t_day,'t_\Omega','Editing','on','HorizontalAlignment','left');
	text(R_o*1.5,t_SV,'t_{SV}','Editing','on','HorizontalAlignment','left','color','c');
	text(R_o*1.5,t_spinup(2),'t_{spin-up}','Editing','on','HorizontalAlignment','left','color','m');
	text(R_o,y_lim(1)/4,'r_{o}','Editing','on','HorizontalAlignment','center');
%	text(l_core_u(1),t_core_u(1),'Re_{l}\sim1','Editing','on','HorizontalAlignment','right');
	text(l_core_u(4),t_core_u(4),'u','Editing','on','HorizontalAlignment','left','color','c');
%	text(l_jet_zonal,t_jet_zonal,'u_{\fontname{Symbol}j}','Editing','on','HorizontalAlignment','left'); % ruse pour avoir \varphi
	text(l_core_b(4),t_core_b(4),'b','Editing','on','HorizontalAlignment','left','color','r');
	text(R_o*1.5,t_A0,'t_{Alfven}','Editing','on','HorizontalAlignment','left','color','r');
% Re(l) = 1
	text(l_core_u(1),t_core_u(1),'$R \; \: e \: ( \, \ell \, ) \sim \; 1 \!\!\!\!$', 'Editing','on','HorizontalAlignment','right','interpreter','latex');
	text(l_core_u(1),t_core_u(1),'$\bigcirc$','Editing','on','HorizontalAlignment','center','FontSize',30,'interpreter','latex');
% Rm(l) = 1
	text(l_core_u(3),t_core_u(3)*0.8,'$\;\;\; R \; \: m \; \; ( \, \ell \, ) \sim \; 1$', 'Editing','on','HorizontalAlignment','left','interpreter','latex');
	text(l_core_u(3),t_core_u(3),'$\bigcirc$','Editing','on','HorizontalAlignment','center','FontSize',30,'interpreter','latex');
% Lu(l) = 1
	[l_Lu, t_Lu] = function_intersect(R_o, t_A0, 1., 1., 1./eta, 2.) ; % tau_Alfven et tau_eta
	text(l_Lu,t_Lu*0.8,'$\quad L \; \: u \; ( \, \ell \, ) \sim \; 1$', 'Editing','on','HorizontalAlignment','left','interpreter','latex');
	text(l_Lu,t_Lu,'$\bigcirc$','Editing','on','HorizontalAlignment','center','FontSize',30,'interpreter','latex');
%	text(l_core_u(1),3*t_core_u(1),'\lambda_{l}\sim1','Editing','on','HorizontalAlignment','right');
% Lehnert(l) = 1
	t_Lehnert = t_day ;
	l_Lehnert = R_o*t_day/t_A0;
	text(l_Lehnert, t_Lehnert*1.5,'$\quad \lambda \; ( \, \ell \, ) \sim \; 1$', 'Editing','on','HorizontalAlignment','left','interpreter','latex');
%	text(l_Lehnert, l_Lehnert,'$\lambda(\ell) \sim 1$','Editing','on','HorizontalAlignment','left','interpreter','latex');
	text(l_Lehnert,t_Lehnert,'$\bigcirc$','Editing','on','HorizontalAlignment','center','FontSize',30,'interpreter','latex');
% labels places a vue
	text(2e2,2e11,'\nu','Editing','on','HorizontalAlignment','center','color','m');
	text(2.2e5,2e11,'\eta','Editing','on','HorizontalAlignment','right','color','b');
% labels a placer :
%	text(l_core_u(2),t_core_u(2),'Ro_{l} \sim 1','Editing','on','HorizontalAlignment','left');
%	text(2*l_core_u(2),t_core_u(2),'$\bigcirc$','Editing','on','HorizontalAlignment','center','FontSize',30,'interpreter','latex');
%	text(x_lim(2),y_lim(1),'\uparrow \delta_{E}','Editing','on','HorizontalAlignment','center');
%	text(2*x_lim(1),y_lim(1),'\uparrow r_{o}E^{1/3}','Editing','on','HorizontalAlignment','center');
%	text(3*x_lim(1),y_lim(1),'\uparrow Rhines scale','Editing','on','HorizontalAlignment','center');
%	text(x_lim(2),2*y_lim(1),'\nu','Editing','on','HorizontalAlignment','right');
	text(x_lim(2),3*y_lim(1),'QG dynamo','Editing','on','HorizontalAlignment','center','Rotation',34);
	text(x_lim(2),4*y_lim(1),'QG MHD','Editing','on','HorizontalAlignment','center','Rotation',0);
%	text(x_lim(2),5*y_lim(1),'\rightarrow','Editing','on','HorizontalAlignment','center','Rotation',34);
	text(x_lim(2),8*y_lim(1),'Rossby','Editing','on','HorizontalAlignment','center','Rotation',-45);
%	text(x_lim(2),2*y_lim(1),'\eta','Editing','on','HorizontalAlignment','right');
	text(x_lim(2),7*y_lim(1),'Alfven','Editing','on','HorizontalAlignment','center','Rotation',45,'color','g');
%	text(x_lim(2),9*y_lim(1),'\rho','Editing','on','HorizontalAlignment','center','Rotation',25);
%	text(l_core_u(1),2*t_core_u(1),'Rm_{l}\sim1','Editing','on','HorizontalAlignment','right');
end
%
xlabel('length scale (m)','fontsize',20,'VerticalAlignment','top');
ylabel('time scale (s)','fontsize',20);
title([planet ' core turbulence'],'fontsize',20)
