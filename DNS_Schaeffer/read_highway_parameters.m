% paramètres utiles pour NS-regime diagrams
% pertinentes pour la simulation numérique 'Highway' mise a l'echelle du noyau (chapitre TOG)
% Numbers from Nathanael
% Numbers defined using d = R_o-R_i as a length scale

Ekman_simu = 1e-7; % nu/Omega/d^2
Lehnert_simu = 1.3e-3;
Rossby_simu = 6e-4; % U_rms/Omega/d
Pm_simu = 0.1; % nu/eta
%
% Omega = 1./(2.*pi*24.*3600.); % s-1 vitesse angulaire de rotation de la Terre sur elle-meme
% 140423 HCN corrige erreur vue par Jerome Noir
Omega = 2.*pi/(24.*3600.); % s-1 vitesse angulaire de rotation de la Terre sur elle-meme
M_o = 1.835e24 ; % kg masse du noyau externe
R_o = 3.480e6 ; % m rayon du noyau
dd = (3.480-1.220)*1e6 ; % m outer core thickness
%
%
nu = Omega*dd^2*Ekman_simu; % m2 s-1 kinematic viscosity
eta = nu./Pm_simu ; % m2 s-1 magnetic diffusivity

% quelques propriétés du noyau (tirées de l'intro de Peter)
kappa = 5.e-6; % m2 s-1 (±3) thermal diffusivity --> nouvelle valeur pour Pr=0.2
kappa_chi = 1.e-9; % m2 s-1 compositional diffusivity (exposant ±2)
alpha = 1.2e-5 ; % K-1 (±0.5) coefficient of thermal expansion
C_p = 850. ; % J kg-1 K-1 (±20) 
%
rho = 10.9e3 ; % kg m^-3 density
gravity = 8 ; % m s-2 gravity
V_P = 9000. ; % m s-1 P-wave velocity
%
% delta_rho = 1.e-9*rho ; % tiré de la co-densité typique de Loper TOG
% delta_rho = 1.e-16*rho ; % pour donner t_SV comme temps convectif si u proportionnelle à Ra^1/2
% delta_rho/rho calculé selon régime
P_diss = 1.e12 ; % W puissance dissipée (incertain)
%B_0 = 3.e-3 ; % T typical magnetic intensity in the core (2-4)
%t_SV = 6.e9*R_o/dd; % s overturn time of the largest vortex (l = R_o) from SV core flow inversion
%t_A0 = R_o/(B_0/sqrt(rho*mu0)) ; % s s temps d'Alfvén pour B_0 pour rayon noyau
%V_A = B_0/sqrt(rho*mu0) ; % vitesse des ondes d'Alfvén pour B_0
%
% from Nathanael's analysis
t_SV = 5.455328363817932606e+08; % s overturn time of the largest vortex (l = R_o)
t_A0 = 8.124167826082870364e+07; % s temps d'Alfvén pour B_0 pour rayon noyau
V_A = R_o/t_A0; % vitesse des ondes d'Alfvén pour B_0
B0 = V_A*sqrt(rho*mu0);