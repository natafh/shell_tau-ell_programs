# tau-ell_programs



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gricad-gitlab.univ-grenoble-alpes.fr/natafh/shell_tau-ell_programs.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gricad-gitlab.univ-grenoble-alpes.fr/natafh/shell_tau-ell_programs/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## NAME
"tau-ell" regime diagrams for stars and planetary cores

## Description
The interior of planets and stars are the seat of rich and complex fluid dynamics, in which the effects of rotation and magnetic field combine. The programs of this repositery build and plot "tau-ell" regime diagrams of stars and planetary cores. "tau-ell" diagrams offer a synthetic picture of the dynamical regime of a given object.

Introduced by Nataf & Schaeffer in the second edition of the "Treatise on Geophysics" in 2015, "tau-ell" diagrams are further described and exploited in Nataf & Schaeffer (2024).
The current package of python programs was used to produce all figures of the latter article.
One can also construct "tau-ell" diagrams of direct numerical simulations (DNS) and Laboratory experiments. Examples of these are included in the package.

"tau-ell" regime diagrams provide a very intuitive representation of several concepts used in geophysical and astrophysical fluid dynamics, such as dimensionless numbers, force balances, path strategies, etc. 

## Visuals
Here are examples of "tau-ell" diagrams built with this package for the Earth's core, for the S2 dynamo DNS of Schaeffer et al (2017), and for the DTS liquid sodium MHD experiment (see Brito et al, 2011).

![Image Alt text](/Figures/Earth_QG-MAC_JA_dynamo_turbulence.png "tau-ell regime diagram for the Earth's core"))
![Image Alt text](/Figures/S2_DNS_dynamo_turbulence.png "tau-ell regime diagram for the S2 dynamo simulation of Schaeffer et al (2017)")
![Image Alt text](/Figures/DTS_experiment_MHD_turbulence.png "tau-ell regime diagram for the DTS liquid sodium MHD experiment"))

## Installation
The package requires a Python 3 installation, including matplotlib and numpy packages.
Simply donwload all files of the package in a folder of your choice. You may need to customize the first line of the Python files (currently: "#!/usr/bin/env python3" or "#!/opt/anaconda3/bin/python").

## Usage
You may simply run the plot_object.py script to build the "tau-ell" regime diagram of a listed object.
Customize the choice of object and options in the first paragraph of the plot_object.py script (between the '>>>>' and '<<<<' marks).
Objects are listed and described in read_parameters.py. You may add your own objects, documenting the needed properties, following the examples of presently available objects.
Choosing the "template" option, you get a template of the object that sets the scene for testing or building scenarios of its dynamical regime (given by the timescale versus length-scale evolution of its three field variables: velocity, density, and magnetic field).
A number of "scenarios" are listed and exposed in the plot_scenario.py function, which rely on various choices of force balance (CIA, QG-CIA, QG-VAC, MAC, QG-MAC, IMAC) and additional choices (such as spectral slopes). You may invent and include your own scenario in that file.

Scripts plot_DNS.py and plot_experiment.py build the "tau-ell" diagram of a numerical simulation and a Lab experiment, respectively. One needs to provide the corresponding spherical harmonic spectra of energies of the DNS, or wavenumber spectra for the experiment. These scripts work well for the provided examples (DNS_Schaeffer, DNS_Guervilly for plot_DNS.py, and experiment_DTS for plot_experiment), but might need some customization for other results (see Nataf & Schaeffer, 2024 for instructions).

Scripts plot_Kolmogorov.py and plot_convection_onset.py are short scripts that can be used for pedagogical purposes.

File tau_ell_lib.py gathers a number of shared python functions called by "tau-ell" programs.

## Support
Please send a message to Henri-Claude.Nataf@univ-grenoble-alpes.fr for help.

## Roadmap
The "tau-ell" approach rests upon a fair number of assumptions and approximations. The programs might evolve following the corrections and extensions that will show up.

## Contributing
Users are welcome to contribute new objects in the read_parameters.py file, and new scenarios in the plot_scenario.py file, following the given instructions, and ensuring appropriate description and origin. Please provide figures of the resulting outcome.
Please keep other customizations for your own.

## Authors and acknowledgment
"tau-ell" python programs were written by Henri-Claude Nataf. Nathanaël Schaeffer provided spectra from several numerical simulations, provided in the package. Quentin Noraz provided guidelines and input for extension to the Sun and stars.

## License
License xxx.
Please cite the following article if using this package:
Nataf H-C. and N. Schaeffer, Dynamic regimes in planetary cores: "tau-ell" diagrams, Comptes-Rendus Geoscience, xxx, 2024.

## Project status
The "tau-ell" community has still to be built! Feel free to join!
